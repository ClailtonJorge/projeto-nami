﻿using UnityEngine;
using System.Collections;

namespace ProjetoNami.Model
{

    public class Fase {

        public int id { get; set; }
        public string nome { get; set; }
        public int tempoFinal { get; set; }
        public float atencao { get; set; }
        public float concentracao { get; set; }
        public System.DateTime dataRealizada { get; set; }
        public int idUser { get; set; }

    }

}
