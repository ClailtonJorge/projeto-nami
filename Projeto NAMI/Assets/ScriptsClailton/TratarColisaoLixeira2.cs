﻿using UnityEngine;
using System.Collections;

public class TratarColisaoLixeira2 : MonoBehaviour {

    private bool executarAnim;
    GameObject objetoAnim;
    float time;
    public ScriptMovimentarPontoDeLuz movimentoPonto;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (executarAnim)
        {
            time += Time.deltaTime;
            if (time <= 5)
            {
                objetoAnim.transform.position = new Vector3(objetoAnim.transform.position.x, objetoAnim.transform.position.y - 0.1f, objetoAnim.transform.position.z);
            }
            else
            {
                executarAnim = false;
                time = 0;
            }
        }
    }


    void OnCollisionEnter(Collision c)
    {
        Debug.Log(c.gameObject.tag);
        if (c.gameObject.name.Equals("inter3"))
        {
            //Animation anim = c.gameObject.GetComponent(typeof(Animation)) as Animation;
            //anim.Play();
            //Animator an = c.gameObject.GetComponent(typeof(Animator)) as Animator;
            //an.Play("AnimacaoCaindo", -1);
            objetoAnim = c.gameObject;
            executarAnim = true;
        }
        else if (c.gameObject.name.Equals("inter2"))
        {
            Debug.Log("Entrou no else");
            TratarColisaoCubo scriptCubo = c.gameObject.GetComponent(typeof(TratarColisaoCubo)) as TratarColisaoCubo;
            movimentoPonto.retirarObjeto();
            scriptCubo.voltarPosicaoInicial();
        }


    }

}
