﻿using UnityEngine;
using System.Collections;
using Leap;

public class ScriptMovimentarPontoDeLuz : MonoBehaviour {

   private GameObject objetoCuboSelecionado;

    public HandController controller;
    private Frame frame;

	// Use this for initialization
	void Start () {
        
	    
	}
	
	// Update is called once per frame
	void Update () {

        frame = controller.GetFrame();

        if (objetoCuboSelecionado != null)
        {
            verificarPosicao(frame, objetoCuboSelecionado);
        }
        
	}


    private void verificarPosicao(Frame frame, GameObject pontoDeLuz)
    {

        foreach (var h in frame.Hands)
        {
            if (h.IsRight)
            {
                Leap.Vector position = h.PalmPosition;
                Vector3 unityPosition = position.ToUnityScaled(false);
                Vector3 worldPosition = controller.transform.TransformPoint(unityPosition);
                pontoDeLuz.transform.position = new Vector3(worldPosition.x, pontoDeLuz.transform.position.y, worldPosition.z);                    
                
            }
        }

    }


    public void adicionarObjeto(GameObject objeto)
    {
        this.objetoCuboSelecionado = objeto;
    }

    public void retirarObjeto()
    {
        this.objetoCuboSelecionado = null;
    }

}
