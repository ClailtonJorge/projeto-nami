﻿using UnityEngine;
using System.Collections;

public class PegarDados : MonoBehaviour {

	Leap.Controller controller;


	// Use this for initialization
	void Start () {
		controller = new Leap.Controller ();
		controller.EnableGesture (Leap.Gesture.GestureType.TYPEKEYTAP);
		controller.Config.SetFloat ("Gesture.KeyTap.MinDownVelocity", 100f);
		controller.Config.SetFloat ("Gesture.KeyTap.HistorySeconds", 0.3f);
		controller.Config.Save ();
	}

	Leap.Finger MovimentoDedos (Leap.FingerList fingerList)
	{
		for (int i = 0; i < fingerList.Count; i++) {
			Leap.Finger finger = fingerList [i];
			//Debug.Log("Dedo esta extendido: "+ finger.IsExtended + " Numero do dedo: "+ finger.Id);
			if (finger.IsExtended) {
				/*Leap.Vector direcao = finger.Direction;
				float trans = Time.deltaTime * direcao.x;
				if (direcao.x > 0) {
					cubo.transform.position = new Vector3 (trans, cubo.transform.position.y, cubo.transform.position.z);
					Debug.Log (direcao.x);
				} else {
					cubo.transform.position = new Vector3 (trans, cubo.transform.position.y, cubo.transform.position.z);
				}*/
				return finger;
			}
		}
		return null;
	}
	
	// Update is called once per frame
	void Update () {
		Leap.Frame frame = controller.Frame ();
		Leap.InteractionBox box = frame.InteractionBox;
		//Debug.Log ("Centro interaction box: "+ box.Height);

		Leap.GestureList gestureList = frame.Gestures();
		Leap.FingerList fingerList = frame.Fingers;


		for(int i = 0; i < gestureList.Count; i++) {
			Leap.Gesture gesto = gestureList[i];

			if(gesto.Type.Equals(Leap.Gesture.GestureType.TYPEKEYTAP)){
				//cubo.transform.position = new Vector3(cubo.transform.position.x + 1, cubo.transform.position.y, cubo.transform.position.z);
				Leap.Finger finger = MovimentoDedos(fingerList);
				//selecionarCubo(cubo.transform.position.x, cubo.transform.position.y, finger.TipPosition.x, finger.TipPosition.y);
				Debug.Log("Gesto de KeyTap executado: "+ finger.Direction.x + " : ");
				GameObject cubo2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
				cubo2.AddComponent<Rigidbody>();
				cubo2.transform.position = new Vector3(finger.TipPosition.x, finger.TipPosition.y, finger.TipPosition.z);
			}

		}

		//MovimentoDedos (fingerList);

	}

	void selecionarCubo(float xCubo, float yCubo, float xDedo, float yDedo) {

		if(xDedo > xCubo + 1 && xDedo < xCubo - 1) {
			//cubo.transform.position = new Vector3(cubo.transform.position.x, cubo.transform.position.y + 1, cubo.transform.position.z);
		}


	}

}
