﻿using UnityEngine;
using System.Collections;
using Leap;

public class TesteInteractionBox : MonoBehaviour {

	Controller controller;
	Frame frame;
	public GameObject planoPiso;
	float x;

	float xDedo;
	float yDedo;

	Leap.Vector vetorPraUsado;

	// Use this for initialization
	void Start () {

		controller = new Controller ();

		controller.EnableGesture (Gesture.GestureType.TYPEKEYTAP);
		controller.Config.SetFloat ("Gesture.KeyTap.MinDownVelocity", 100f);
		controller.Config.SetFloat ("Gesture.KeyTap.HistorySeconds", 0.3f);
		controller.Config.Save ();


	
	}
	
	// Update is called once per frame
	void Update () {		
		frame = controller.Frame ();
		InteractionBox box = frame.InteractionBox;
		Debug.Log("Centro X: "+ box.Center.x + " Centro Y: "+ box.Center.y + " Centro Z: "+ box.Center.z);
		tratarGestureKeyTap (frame);
		tratarPlanoPiso ();


	}

	void tratarGestureKeyTap(Frame frame) {
		GestureList gestos = frame.Gestures ();
		Pointable pointable = frame.Pointables.Frontmost;

		for(int i = 0; i < gestos.Count; i++) {
			Gesture gesto = gestos[i];

			if( gesto.Type.Equals(Gesture.GestureType.TYPEKEYTAP)) {
				Debug.Log("Executou o gesto de KeyTap");
				GameObject cubo2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
				cubo2.AddComponent<Rigidbody>();
				cubo2.transform.position = new Vector3(22.78f, 1.0714962f, 1.258307f);
				//tratarPlanoPiso();
				//tratarInteractionBox (frame);
			}

		}

	}

	void tratarPlanoPiso() {
		MeshRenderer mesh = planoPiso.GetComponent (typeof(MeshRenderer)) as MeshRenderer;
		Debug.Log ("Bounds do planoPiso: "+ mesh.bounds.size);
		x = mesh.bounds.size.x * 0.04f;
	}

	void tratarInteractionBox(Frame frame) {
		InteractionBox box = frame.InteractionBox;
		Pointable pointable = frame.Pointables.Frontmost;


		Leap.Vector leapPoint = pointable.StabilizedTipPosition;
		Leap.Vector normalizedPoint = box.NormalizePoint (leapPoint, false);

		Leap.Vector vetorLeapWorld = leapToWorld (leapPoint, box);
		Debug.Log ("Valores do interactionbox: "+ box.Width);

		xDedo = vetorLeapWorld.x * x;

		GameObject cubo2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
		cubo2.AddComponent<Rigidbody>();
		cubo2.transform.position = new Vector3(xDedo, 5, 0f);
		
	}

	Leap.Vector leapToWorld(Leap.Vector leapPoint, InteractionBox iBox)
	{
		//leapPoint.z *= -1.0f; //right-hand to left-hand rule
		Leap.Vector normalized = iBox.NormalizePoint(leapPoint, false);
		normalized += new Leap.Vector(0.5f, 0f, 0.5f); //recenter origin
		return normalized * 100.0f; //scale
	}





}
