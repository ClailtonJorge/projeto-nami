﻿using UnityEngine;
using System.Collections;
using Leap;

public class TesteCodeProject : MonoBehaviour {

	public HandController handController;
	private HandModel handModel;
	Frame currentFrame, lastFrame, thisFrame = null;

	public GameObject cubePrefab;
	private GameObject cube = null;
	long difference = 0;


	// Use this for initialization
	void Start () {
		handController.GetLeapController ().EnableGesture (Gesture.GestureType.TYPECIRCLE);
		handController.GetLeapController ().EnableGesture (Gesture.GestureType.TYPESWIPE);
		handController.GetLeapController ().EnableGesture (Gesture.GestureType.TYPEKEYTAP);
	}
	
	// Update is called once per frame
	void Update () {


		this.currentFrame = handController.GetFrame ();
		tratarGestureKeyTap (currentFrame);
	//	Vector3 posCubo = pegarPosicao (this.currentFrame);
	//	cubePrefab.transform.position = new Vector3 (posCubo.x, cubePrefab.transform.position.y, cubePrefab.transform.position.z);

		/*
		GestureList gestos = this.currentFrame.Gestures ();
		foreach(Gesture g in gestos) {

			if(g.Type == Gesture.GestureType.TYPECIRCLE) {
				if(this.cube == null) {
					this.cube = GameObject.Instantiate(this.cubePrefab, this.cubePrefab.transform.position, this.cubePrefab.transform.rotation) as GameObject;
				}
			}

			if(g.Type == Gesture.GestureType.TYPESWIPE) {
				if(this.cube != null) {
					Destroy(this.cube);
					this.cube = null;
				}
			}

		}

		foreach (var h in handController.GetFrame().Hands) {
			if(h.IsRight) {
				if(this.cube != null) {
					this.cube.transform.rotation = Quaternion.Euler(h.Direction.Pitch, h.Direction.Yaw, h.Direction.Roll);
				}

				foreach(var f in h.Fingers) {
					if(f.Type == Finger.FingerType.TYPE_INDEX) {
						Leap.Vector position = f.TipPosition;
						Vector3 unityPosition = position.ToUnityScaled(false);
						Vector3 worldPosition = handController.transform.TransformPoint(unityPosition);
					}
				}

			}
		
		}
		*/


	}


	void tratarGestureKeyTap(Frame frame) {
		GestureList gestos = frame.Gestures ();
		Pointable pointable = frame.Pointables.Frontmost;
		
		for(int i = 0; i < gestos.Count; i++) {
			Gesture gesto = gestos[i];
			
			if( gesto.Type.Equals(Gesture.GestureType.TYPEKEYTAP)) {
				Debug.Log("Executou o gesto de KeyTap");
				GameObject cubo2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
				cubo2.AddComponent<Rigidbody>();
				cubo2.transform.position = pegarPosicao(frame);
				//tratarPlanoPiso();
				//tratarInteractionBox (frame);
			}
			
		}
		
	}


	Vector3 pegarPosicao(Frame frame) {
		foreach (var h in frame.Hands) {
			if(h.IsRight) {
				
				foreach(var f in frame.Fingers) {
					if(f.Type == Finger.FingerType.TYPE_INDEX) {
						Leap.Vector position = f.TipPosition;
						Vector3 unityPosition = position.ToUnityScaled(false);
						Vector3 worldPosition = handController.transform.TransformPoint(unityPosition);
						return worldPosition;
					}
				}
				
			}
			
		}

		return new Vector3();
	}


}
